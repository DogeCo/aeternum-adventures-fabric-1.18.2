# Last Update
6/8/2022

A modded server using Fabric and Minecraft version 1.18.2

# Some notes
I haven't fully tested everything yet. However, everything runs fine on my end. I'm using `OpenJDK16` as my base Java as well. 

Additionally, with the `mod menu`, you are free to edit any mod that says **client**.


# Installation Instructions
Go here to grab the Fabric Installer: https://fabricmc.net/use/

### If you are planning on hosting, read here
#### FOR WINDOWS
- Execute the installer and install as **server**. Make sure you use version **1.18.2** as that is the version that these mods support.
- This will begin downloading the server install for the version you selected.

#### FOR LINUX/UBUNTU
- Follow this guide: https://fabricmc.net/wiki/player:tutorials:install_server

### If you are planning on playing on the server, read here
- Ensure you delete everything inside your `.minecraft` folder (WIN_KEY+R -> type "%APPDATA%" to quickly get to the folder that contains the `.minecraft` folder) so there is a clean start.
- After deleting everything, **Relaunch the minecraft launcher**. Log back in and **download the 1.18.2 version of Minecraft**
- Launch the vanilla version of 1.18.2 and get to the menu just to make sure Minecraft loads properly
- **If something borks in the steps above, you can't progress. Redo the steps and make sure you can launch the base game!**
- Execute the Fabric Installer that you downloaded in the `Installation Instructions` section and install as **client**. Make sure you use version **1.18.2** as that is the version that these mods support.
- Within the Minecraft Launcher window, navigate to the `Installations` tab, move your mouse over the `Fabric-loader-1.18.2` installation and click on the `...` button. Click `Edit`

![JVM arguments](https://i.imgur.com/NZRJwxD.png)

Under the JVM Arguments section, paste the following: `-Xmx8G -XX:+UnlockExperimentalVMOptions -XX:+UseG1GC -XX:G1NewSizePercent=20 -XX:G1ReservePercent=20 -XX:MaxGCPauseMillis=50 -XX:G1HeapRegionSize=32M`

#### **Please note that the `-Xmx8G` is SPECIFIC to your system. It is telling Minecraft how much RAM it can use. If you don't have 8GB of free RAM, then lower this number to something your system can utilize. I recommend not going lower than 4GB.**


- Download and copy the `config`, `mods`, `resourcepacks`, and `shaderpacks` from this site (Or clone if you know how to use git!) and place them into the `.minecraft` folder
- Copy the contents of `client-mods` and place them into the `mods` folder.


# Rules, Suggestions, and Information

### Rules 

1. There is no PvP because this server is \#Balanced o7. At some point, you will probably 1-shot other people so-

![Wow 504 damage in minecraft](https://i.imgur.com/oDzWh2g.png)

2. Don't intentionally lag the server.

### Suggestions

1. This server is moreso focused on building visually appealing stuff as a lot of mods focus on customization and automation. 
2. Watch out for Elite mobs. If you're just starting, you probably will struggle fighting a low tier elite. Anything higher will probably murder you. 
3. Use REI's (Roughly Enough Items) interface to explore things to craft and whatnot. 

![REI Interface](https://i.imgur.com/qKZGIKb.png)

The right side will show all items available in game. The search bar will filter based on what you search. If you type `@` before a word, it can search by category. For example, if you search `@Macaw`, it will display all the items in Macaw's mods. Pressing `A` when you highlight an item will place it on the left side as a "Favorites" seciton.

4. Build and make interesting things! Recommended mods for decorations:
  a. Macaw's Items
  b. Adorn
  c. Lightest Lamps
  d. Chipped
  e. Chisel
  f. Bits and Chisel
5. Running out of inventory space all the time? Look up `@Inmis` and `Bag of holding` on REI.
6. Furnaces taking too long? Look up `@Fabric Furnace`
7. Sad that your chests can't hold more items? Look up `@Iron Chests` and `Crude Storage` Also look up `Dolly` if you wanna move chests without breaking the chest and dropping all the contents (ONLY WORKS ON VANILLA CHESTS AND IRON CHESTS MOD CHESTS)
8. ### When picking your Origin at the very beginning, I **STRONGLY** suggest playing as HUMAN. I am not responsible for the pain and suffering for picking a different origin. Especially Phantom. Do not pick Phantom.

### Information

- This server is ran on my own local server. In exchange for better performace, the server may or may not go down at times. 
- I might push updates that will require you to download new things
- This server's day last twice as long as dark

# Getting Started

### First Load

The game may or may not give you server lag (can move but can't interact) on the very first time you load into the server because it unlocks a boatload of stuff and whatnot. You'll know when you're done loading with the `Pick your origins` screen appears.

It is also worth noting that starting area is spawn protected. Follow the sign instructions and take a quick look before exiting. 

### Keybinds
I recommend starting a basic single player world in creative mode so you can adjust the keybindings! But if not, that's fine. The server spawns you in a safe location. 

This modded server allows you to add modifiers to your keybinds (EX: CTRL+E, SHIFT+E, ALT+E). There are going to be a lot of initial keybind conflicts, so make sure you modify everything. There is a searchbar within the keybind screen and example patterns to filter.

EX: If you want to see what is occupying the "R" key, you would type `@k r`

![filter by R key](https://i.imgur.com/G8rMwCO.png)

EX: If you know the mod name and want to see what is there, you would type @c <mod name>. 

![filter by mod name](https://i.imgur.com/MYwiii8.png)

Some essentials you may want to consider keybinding (or fixing):
- `Excavate`
- `Open Backpack`
- `Xaero's Minimap / World Map`

### HP Level Progression System
There is a level progression system for HP values only. Once a specific mod gets updated, it will replace the HP leveling mod. The max level you can reach is lvl 90. That is about 5 HP bar's worth of health. In order to level up, just fill the red EXP bar. It acumulate with regular EXP.

**If you are wondering why you have less hearts than usual, it is because of this mod. If you think you are going to die too easily... good luck!**
![HP EXP](https://i.imgur.com/R0F0cqv.png)

### Veinmining
Veinmining is controversial since it diminishes the feeling of mining progression, but I like having it. However, in this server, veinmining will result in a 3x stamina consumption multiplier. (Keybind name is called `Excavate`)

### Biome Information
A small white text appears whenever you go to a new biome. Unfortunately, since it is client sided, and everyone has different monitor configurations, there is no nice 'catch-all' solution. Instead, you will have to go to the `ModMenu` (ESC -> Mods) and search up `biomeinfo` and click on the small edit button in order to configure it to a position that you like.

### Inventory HUD
If you see your inventory items on your screen above your hotbar, press `I` (default keybind). Press `O` (default keybind) to adjust the inventory HUD settings. You can also do this through the `ModMenu` by searching `InventoryHUD`.


# What Next?

### What do I even do? How do I progress?

Tbh, idk either. You are free to play the game as vanilla as you want and just appreciate the additional biomes and effects.

If I had to say what exactly you could focus on... google information on these mods as they are probably one of the biggest mods in the server:

- Applied Energistics 2 (Tech and automation)
- Botania (Planty-tech and automation. Pseudo magic.)
- Croparia (Plants and utilizing elements to make new stuff. Magic.)
- Spectrum (Use the colors to make new discoveries of the world. Magic. I also have no real idea how to start this mod. I just know that you have to find some crystal or something xD)
- Minecraft Dungeons Weapons and Armor (Mostly for just weapons and armor that you can craft)
- Gobber2 (Powerful weapons, armor, and food meant to extend vanilla MC progression)

Alternatively, you can use REI (Roughly Enough Items, the screen that appears on your right when you open your inventory, to seearch for these mod items. Just type in `@Botania` for example in the search bar in REI)

### ModMenu
Pressing ESC will show an extra button called `Mods`. This is the `ModMenu`. More importantly, it allows you to customize values in the `config` folder that you would otherwise have to manually adjust with a text editor. 

**IN ORDER TO PREVENT YOURSELF FROM GETTING BOOTED FROM THE SERVER DUE TO INCOMPATIBILITY, PLEASE ONLY MODIFY MODS THAT SAY `CLIENT` Most mods will not actually do anything if you adjust a non-client mod, but just in case!**

Not every mod is editable, but if they are, you will see a small `settings` button at the top right. 

![edit mods](https://i.imgur.com/iJeMXvJ.png)

### Visuals

There are some shaders and texturepacks available to download and utilize. While there are multiple choices, the optimal way would be to follow this screenshot for the order:

![texturepack order](https://i.imgur.com/QpNUzvx.png)

Unfortunately, I cannot quite figure out how to get Better_Dogs and mrlm-s-cats to work properly, but hopefully I can figure out the proper config options for that. 

`Stay True` and `FaithfulPBR` are the 2 base texture packs. `Stay True` keeps the same style but adds some extra features like glowing eyes and whatnot. `FaithfulPBR` is literally the same thing, but the resolution is doubled to 32x32 and includes Normal Mapping and Reflections for shaders.

TL;DR: If you plan to use shaders, it would be better to place `Faithful` above `Stay True` (or omit `Stay True` entirely). If you do not plan to use shaders, it would be better to stick with `Stay True`

As for shaders, it is important to note that we are NOT using Optifine, which has been the standard base for basically anything that graphically overhauled Minecraft. We are using Iris Shaders and a few other optimization mods instead. As a result, while most of these shaders work (`Kappa` does not work at all. Don't use it.), Iris does not support texturepacks that offer Normal Mapping or Specular mapping natively. **Due to this, the only way you can see those nice reflections and proper glow is to use `ComplementaryShaders` and ensure `Integrated PBR+` is selected within the shaderpack settings for `RP Support`.**

### Visual effects and sounds

There are a good amount of client-sided mods that try to enhance the sounds and particle effects displayed in game. However, if you do not want to see them or change the effects, go through the `Modmenu` and search up each of these mods individually to adjust:

- **Biome Info** (Fades in and fades out white text about the biome you're in. Probably gonna have to adjust placement!)
- **Biome Weather Effect** (Adds new rain visual and different weather patterns based on biome. Can also adjust intensity)
- **Effective** (Adds water splashes, waterfalls, and small water droplet splashes)
- **Illuminations** (Adds ambient lighting at night in the form of fireflies and glow worms)
- **PrescenceFootsteps** (Gives your character footsteps sound effects based on material type. I honestly can't play without this anymore xD)
- **Torohealth** (Shows damage value indicator when attacking something. There is more to display but I have them disabled since other things already take care of that. But you are free to enable if you want.)
- **Charmonium** (Ambient sound effects. IE: Birds chirping, crickets, etc)
- **Xaero's Minimap** (Can adjust what gets displayed and where the minimap is located on the screen)
- **wthit** (What the Hell is that?) (Displays block and entity information when you look at it on the top center of your screen.)

### Character Appearance

This server uses the Cosmetica mod, which allows you to use custom capes, hats, and shoulder pets! In order to get there, do `ESC -> Options -> Cosmetica Settings...`

Please note that you have to log off and log back in for others to see. 

# FAQ
**Q: I can hear some entities but I cannot see them.**

This is due to the FreshAnimations ResourcePack. Removing it from the loaded textures in the options menu should fix it. 

**Q: The ambient noises are too loud.**

The ambient noises come from the Charmonium mod/resourcepack. If you don't want extra ambient noises at all, remove the resourcepack from the loaded textures in the options menu. If you want them quieter, you are able to configure the values within the `ModMenu`. 

**Q: I am lagging like crazy around heavily forested areas.**

Try removing the `Better Leaves` resourcepack.

**Q: There is an annoying red beam of light showing up at where I died. How do I get rid of it?**

![annoying red light](https://i.imgur.com/FIHDrDw.png)
Unfortunately I cannot push out a config change for this. However, in order to resolve this, press E to open your inventory. Click on the icon in the picture below:

![click here](https://i.imgur.com/OrOx7nQ.png)

From there, it will load up the FTB Chunk map. Click on the gear icon at the bottom left, then set `Show waypoints in world` to `False`. Then click on the green checkbox at the top right.

That will disable future red beams of light from generating in the future. To disable the current beam of light, go back to the FTB Chunk map and look for a red `X`. Right click on it, and delete it. That was your death marker.  
